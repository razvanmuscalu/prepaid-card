package com.curve.card.http

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes.{BadRequest, NotFound, OK}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.curve.card.BaseTest
import com.curve.card.model._
import com.curve.card.service.MerchantService
import org.mockito.Mockito._
import org.scalatest.BeforeAndAfterEach
import org.scalatest.mockito.MockitoSugar
import spray.json._
import org.mockito.ArgumentMatchers._

import scala.concurrent.Future

class MerchantRouterTest extends BaseTest with ScalatestRouteTest with MockitoSugar with BeforeAndAfterEach {
  private val testMerchantId = "uuid-1"
  private val testPayment = read("/mocks/payment.json").parseJson
  private val testPaymentOutcome = PaymentOutcome("test", "uuid")

  private val testCaptureRequest = read("/mocks/capture-request.json").parseJson
  private val testCaptureOutcome = CaptureOutcome("test", 50, 50)

  private val testMerchantAccount = MerchantAccount(List(PaymentTransaction(50, "uuid")))

  private val merchantService = mock[MerchantService]
  private val route: Route = MerchantRouter.routes(merchantService)

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    reset(merchantService)
    // default mocks (overridden in individual tests where needed)
    when(merchantService.accept(any[String], any[Payment])).thenReturn(Future.successful(testPaymentOutcome))
    when(merchantService.capture(any[String], any[CaptureRequest])).thenReturn(Future.successful(testCaptureOutcome))
    when(merchantService.reverse(any[String], any[CaptureRequest])).thenReturn(Future.successful(testCaptureOutcome))
    when(merchantService.get(any[String])).thenReturn(Future.successful(Some(testMerchantAccount)))
  }

  "MerchantRouter" should {
    "return 404 NotFound when asking for non-existent endpoint" in {
      Post("/unknown") ~> Route.seal(route) ~> check {
        status shouldBe NotFound
      }
    }
  }

  "MerchantRouter /accept" should {
    "return 200 OK" in {
      when(merchantService.accept(testMerchantId, testPayment.convertTo[Payment])).thenReturn(Future.successful(testPaymentOutcome))
      Post(s"/merchant/$testMerchantId/accept", testPayment) ~> Route.seal(route) ~> check {
        status shouldBe OK
      }
    }

    "return 400 BadRequest when asking with no request body" in {
      Post(s"/merchant/$testMerchantId/accept") ~> Route.seal(route) ~> check {
        status shouldBe BadRequest
      }
    }

    "return 400 BadRequest when asking with invalid request body" in {
      Post(s"/merchant/$testMerchantId/accept", read("/mocks/invalid-payment.json").parseJson) ~> Route.seal(route) ~> check {
        status shouldBe BadRequest
      }
    }

    "call service with request body" in {
      Post(s"/merchant/$testMerchantId/accept", testPayment) ~> Route.seal(route) ~> check {
        verify(merchantService).accept(testMerchantId, testPayment.convertTo[Payment])
      }
    }

    "return response" in {
      when(merchantService.accept(testMerchantId, testPayment.convertTo[Payment])).thenReturn(Future.successful(testPaymentOutcome))
      Post(s"/merchant/$testMerchantId/accept", testPayment) ~> Route.seal(route) ~> check {
        responseAs[JsValue].convertTo[PaymentOutcome] shouldBe testPaymentOutcome
      }
    }
  }

  "MerchantRouter /capture" should {
    "return 200 OK" in {
      when(merchantService.capture(testMerchantId, testCaptureRequest.convertTo[CaptureRequest])).thenReturn(Future.successful(testCaptureOutcome))
      Post(s"/merchant/$testMerchantId/capture", testCaptureRequest) ~> Route.seal(route) ~> check {
        status shouldBe OK
      }
    }

    "return 400 BadRequest when asking with no request body" in {
      Post(s"/merchant/$testMerchantId/capture") ~> Route.seal(route) ~> check {
        status shouldBe BadRequest
      }
    }

    "return 400 BadRequest when asking with invalid request body" in {
      Post(s"/merchant/$testMerchantId/capture", read("/mocks/invalid-capture-request.json").parseJson) ~> Route.seal(route) ~> check {
        status shouldBe BadRequest
      }
    }

    "call service with request body" in {
      Post(s"/merchant/$testMerchantId/capture", testCaptureRequest) ~> Route.seal(route) ~> check {
        verify(merchantService).capture(testMerchantId, testCaptureRequest.convertTo[CaptureRequest])
      }
    }

    "return response" in {
      when(merchantService.capture(testMerchantId, testCaptureRequest.convertTo[CaptureRequest])).thenReturn(Future.successful(testCaptureOutcome))
      Post(s"/merchant/$testMerchantId/capture", testCaptureRequest) ~> Route.seal(route) ~> check {
        responseAs[JsValue].convertTo[CaptureOutcome] shouldBe testCaptureOutcome
      }
    }
  }

  "MerchantRouter /reverse" should {
    "return 200 OK" in {
      when(merchantService.reverse(testMerchantId, testCaptureRequest.convertTo[CaptureRequest])).thenReturn(Future.successful(testCaptureOutcome))
      Post(s"/merchant/$testMerchantId/reverse", testCaptureRequest) ~> Route.seal(route) ~> check {
        status shouldBe OK
      }
    }

    "return 400 BadRequest when asking with no request body" in {
      Post(s"/merchant/$testMerchantId/reverse") ~> Route.seal(route) ~> check {
        status shouldBe BadRequest
      }
    }

    "return 400 BadRequest when asking with invalid request body" in {
      Post(s"/merchant/$testMerchantId/reverse", read("/mocks/invalid-capture-request.json").parseJson) ~> Route.seal(route) ~> check {
        status shouldBe BadRequest
      }
    }

    "call service with request body" in {
      Post(s"/merchant/$testMerchantId/reverse", testCaptureRequest) ~> Route.seal(route) ~> check {
        verify(merchantService).reverse(testMerchantId, testCaptureRequest.convertTo[CaptureRequest])
      }
    }

    "return response" in {
      when(merchantService.reverse(testMerchantId, testCaptureRequest.convertTo[CaptureRequest])).thenReturn(Future.successful(testCaptureOutcome))
      Post(s"/merchant/$testMerchantId/reverse", testCaptureRequest) ~> Route.seal(route) ~> check {
        responseAs[JsValue].convertTo[CaptureOutcome] shouldBe testCaptureOutcome
      }
    }
  }

  "MerchantRouter (retrieve)" should {
    "return 200 OK" in {
      when(merchantService.get(testMerchantId)).thenReturn(Future.successful(Some(testMerchantAccount)))
      Get(s"/merchant/$testMerchantId") ~> Route.seal(route) ~> check {
        status shouldBe OK
      }
    }

    "call service" in {
      Get(s"/merchant/$testMerchantId") ~> Route.seal(route) ~> check {
        verify(merchantService).get(testMerchantId)
      }
    }

    "return response" in {
      when(merchantService.get(testMerchantId)).thenReturn(Future.successful(Some(testMerchantAccount)))
      Get(s"/merchant/$testMerchantId") ~> Route.seal(route) ~> check {
        responseAs[JsValue].convertTo[MerchantAccount] shouldBe testMerchantAccount
      }
    }

    "return exception response if merchant not found" in {
      when(merchantService.get(testMerchantId)).thenReturn(Future.successful(None))
      Get(s"/merchant/$testMerchantId") ~> Route.seal(route) ~> check {
        responseAs[JsValue].convertTo[ExceptionResponse] shouldBe ExceptionResponse("Merchant not found")
      }
    }
  }
}
