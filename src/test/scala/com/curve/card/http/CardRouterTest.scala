package com.curve.card.http

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes.{BadRequest, NotFound, OK}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.curve.card.BaseTest
import com.curve.card.model.{CardAccount, ExceptionResponse, LoadRequest}
import com.curve.card.service.CardService
import org.mockito.Mockito._
import org.scalatest.BeforeAndAfterEach
import org.scalatest.mockito.MockitoSugar
import spray.json._
import org.mockito.ArgumentMatchers._

import scala.concurrent.Future

class CardRouterTest extends BaseTest with ScalatestRouteTest with MockitoSugar with BeforeAndAfterEach {
  private val testCardId = "uuid-1"
  private val testLoadRequest = read("/mocks/load-request.json").parseJson
  private val testCardAccount = CardAccount(testCardId, 0, 0)

  private val cardService = mock[CardService]
  private val route: Route = CardRouter.routes(cardService)

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    reset(cardService)
    // default mocks (overridden in individual tests where needed)
    when(cardService.load(any[String], any[LoadRequest])).thenReturn(Future.successful(Some(testCardAccount)))
    when(cardService.create()).thenReturn(Future.successful(testCardAccount))
    when(cardService.get(any[String])).thenReturn(Future.successful(Some(testCardAccount)))
  }

  "CardRouter" should {
    "return 404 NotFound when asking for non-existent endpoint" in {
      Post("/unknown") ~> Route.seal(route) ~> check {
        status shouldBe NotFound
      }
    }
  }

  "CardRouter (load)" should {
    "return 200 OK" in {
      when(cardService.load(testCardId, testLoadRequest.convertTo[LoadRequest])).thenReturn(Future.successful(Some(testCardAccount)))
      Put(s"/card/$testCardId", testLoadRequest) ~> Route.seal(route) ~> check {
        status shouldBe OK
      }
    }

    "return 400 BadRequest when asking with no request body" in {
      Put(s"/card/$testCardId") ~> Route.seal(route) ~> check {
        status shouldBe BadRequest
      }
    }

    "return 400 BadRequest when asking with invalid request body" in {
      Put(s"/card/$testCardId", read("/mocks/invalid-load-request.json").parseJson) ~> Route.seal(route) ~> check {
        status shouldBe BadRequest
      }
    }

    "call service with request body" in {
      Put(s"/card/$testCardId", testLoadRequest) ~> Route.seal(route) ~> check {
        verify(cardService).load(testCardId, testLoadRequest.convertTo[LoadRequest])
      }
    }

    "return response" in {
      when(cardService.load(testCardId, testLoadRequest.convertTo[LoadRequest])).thenReturn(Future.successful(Some(testCardAccount)))
      Put(s"/card/$testCardId", testLoadRequest) ~> Route.seal(route) ~> check {
        responseAs[JsValue].convertTo[CardAccount] shouldBe testCardAccount
      }
    }

    "return exception response if card not found" in {
      when(cardService.load(testCardId, testLoadRequest.convertTo[LoadRequest])).thenReturn(Future.successful(None))
      Put(s"/card/$testCardId", testLoadRequest) ~> Route.seal(route) ~> check {
        responseAs[JsValue].convertTo[ExceptionResponse] shouldBe ExceptionResponse("Card not found, create a card first")
      }
    }
  }

  "CardRouter (create)" should {
    "return 200 OK" in {
      when(cardService.create()).thenReturn(Future.successful(testCardAccount))
      Post("/card") ~> Route.seal(route) ~> check {
        status shouldBe OK
      }
    }

    "call service" in {
      Post("/card") ~> Route.seal(route) ~> check {
        verify(cardService).create()
      }
    }

    "return response" in {
      when(cardService.create()).thenReturn(Future.successful(testCardAccount))
      Post("/card") ~> Route.seal(route) ~> check {
        responseAs[JsValue].convertTo[CardAccount] shouldBe testCardAccount
      }
    }
  }

  "CardRouter (retrieve)" should {
    "return 200 OK" in {
      when(cardService.get(testCardId)).thenReturn(Future.successful(Some(testCardAccount)))
      Get(s"/card/$testCardId") ~> Route.seal(route) ~> check {
        status shouldBe OK
      }
    }

    "call service" in {
      Get(s"/card/$testCardId") ~> Route.seal(route) ~> check {
        verify(cardService).get(testCardId)
      }
    }

    "return response" in {
      when(cardService.get(testCardId)).thenReturn(Future.successful(Some(testCardAccount)))
      Get(s"/card/$testCardId") ~> Route.seal(route) ~> check {
        responseAs[JsValue].convertTo[CardAccount] shouldBe testCardAccount
      }
    }

    "return exception response if card not found" in {
      when(cardService.get(testCardId)).thenReturn(Future.successful(None))
      Get(s"/card/$testCardId") ~> Route.seal(route) ~> check {
        responseAs[JsValue].convertTo[ExceptionResponse] shouldBe ExceptionResponse("Card not found, create a card first")
      }
    }
  }
}
