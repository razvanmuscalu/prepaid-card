package com.curve.card.service

import java.util.UUID

import com.curve.card.SysBaseTest
import com.curve.card.model._
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito._
import org.scalatest.BeforeAndAfterEach
import org.scalatest.mockito.MockitoSugar
import spray.json._

import scala.concurrent.Future

class MerchantServiceTest extends SysBaseTest with MockitoSugar with BeforeAndAfterEach {
  private val testMerchantId = "uuid-1"
  private val testPayment = Payment(50)
  private val testCaptureRequest = CaptureRequest(25, "uuid")
  private val testMerchantAccount = MerchantAccount(List(PaymentTransaction(50, "uuid")))
  private val store = mock[Store]
  private val cardService = mock[CardService]
  private val merchantService = new MerchantServiceImpl(cardService, store)

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    reset(store, cardService)
    // default mocks (overridden in individual tests where needed)
    when(store.get(any[String])).thenReturn(Future.successful(Some(testMerchantAccount.toJson.toString())))
    when(store.put(any[String], any[String])).thenReturn(Future.successful(testMerchantAccount.toJson.toString()))
    when(cardService.authorise(any[String], any[AuthorisationRequest])).thenReturn(Future.successful(AuthorisationResponse(true)))
  }

  "MerchantService.accept" should {
    "attempt to authorise against card" in {
      whenReady(merchantService.accept(testMerchantId, testPayment)) { _ =>
        verify(cardService).authorise(any[String], any[AuthorisationRequest])
      }
    }

    "return successful payment if card authorised the request" in {
      val resp = merchantService.accept(testMerchantId, testPayment).futureValue
      resp.message shouldBe "Payment successful"
    }

    "return failed payment if card did not authorise the request" in {
      when(cardService.authorise(any[String], any[AuthorisationRequest])).thenReturn(Future.successful(AuthorisationResponse(false)))
      val resp = merchantService.accept(testMerchantId, testPayment).futureValue
      resp.message shouldBe "Payment declined"
    }

    "do not update merchant account if card did not authorise the request" in {
      when(cardService.authorise(any[String], any[AuthorisationRequest])).thenReturn(Future.successful(AuthorisationResponse(false)))
      whenReady(merchantService.accept(testMerchantId, testPayment)) { _ =>
        verify(store, never()).put(any[String], any[String])
      }
    }

    "assert that same reference is sent to card and returned in response" in {
      whenReady(merchantService.accept(testMerchantId, testPayment)) { resp =>
        UUID.fromString(resp.reference) // should not throw exception
        verify(cardService).authorise(testMerchantId, AuthorisationRequest(50, resp.reference))
      }
    }

    "update merchant account if payment successful" in {
      whenReady(merchantService.accept(testMerchantId, testPayment)) { resp =>
        val updatedMerchantAccount = MerchantAccount(
          List(PaymentTransaction(50, resp.reference), PaymentTransaction(50, "uuid"))
        )
        verify(store).put(s"merchant.$testMerchantId", updatedMerchantAccount.toJson.toString())
      }
    }

    "throw exception when store has invalid format" in {
      when(store.get(s"merchant.$testMerchantId")).thenReturn(Future.successful(Some("unknown")))
      whenReady(merchantService.accept(testMerchantId, testPayment).failed) { resp =>
        resp shouldBe a[RuntimeException]
        resp.getMessage shouldBe "Something went wrong"
      }
    }

    "initialise new merchant if merchant not found" in {
      when(store.get(s"merchant.$testMerchantId")).thenReturn(Future.successful(None))
      whenReady(merchantService.accept(testMerchantId, testPayment)) { resp =>
        val updatedMerchantAccount = MerchantAccount(
          List(PaymentTransaction(50, resp.reference))
        )
        verify(store).put(s"merchant.$testMerchantId", updatedMerchantAccount.toJson.toString())
      }
    }
  }

  "MerchantService.capture" should {
    "throw exception when store has invalid format" in {
      when(store.get(s"merchant.$testMerchantId")).thenReturn(Future.successful(Some("unknown")))
      whenReady(merchantService.capture(testMerchantId, testCaptureRequest).failed) { resp =>
        resp shouldBe a[RuntimeException]
        resp.getMessage shouldBe "Something went wrong"
      }
    }

    "update merchant account and decrease available amount if capture successful" in {
      whenReady(merchantService.capture(testMerchantId, testCaptureRequest)) { _ =>
        val updatedMerchantAccount = MerchantAccount(
          List(PaymentTransaction(25, "uuid"))
        )
        verify(store).put(s"merchant.$testMerchantId", updatedMerchantAccount.toJson.toString())
      }
    }

    "return success capture" in {
      whenReady(merchantService.capture(testMerchantId, testCaptureRequest)) { resp =>
        resp.message shouldBe "Capture successful"
        resp.captured shouldBe 25
        resp.available shouldBe 25
      }
    }

    "update card amount" in {
      merchantService.capture(testMerchantId, testCaptureRequest)
      Thread.sleep(500)
      verify(cardService).charge(testMerchantId, 25)
    }

    "do not capture if blocked amount less than requested amount" in {
      whenReady(merchantService.capture(testMerchantId, testCaptureRequest.copy(amount = 100))) { resp =>
        verify(store, never()).put(any[String], any[String])
        resp.message shouldBe "Capture failed. Available balance too low."
        resp.available shouldBe 50
      }
    }

    "do not capture if transaction not found" in {
      whenReady(merchantService.capture(testMerchantId, testCaptureRequest.copy(paymentReference = "unknown"))) { resp =>
        verify(store, never()).put(any[String], any[String])
        resp.message shouldBe "Capture failed. Transaction not found"
      }
    }

    "throw exception when merchant does not exist" in {
      when(store.get(s"merchant.$testMerchantId")).thenReturn(Future.successful(None))
      whenReady(merchantService.capture(testMerchantId, testCaptureRequest).failed) { resp =>
        resp shouldBe a[RuntimeException]
        resp.getMessage shouldBe "Merchant not found"
      }
    }
  }

  "MerchantService.reverse" should {
    "throw exception when store has invalid format" in {
      when(store.get(s"merchant.$testMerchantId")).thenReturn(Future.successful(Some("unknown")))
      whenReady(merchantService.reverse(testMerchantId, testCaptureRequest).failed) { resp =>
        resp shouldBe a[RuntimeException]
        resp.getMessage shouldBe "Something went wrong"
      }
    }

    "update merchant account and decrease available amount if reverse successful" in {
      whenReady(merchantService.reverse(testMerchantId, testCaptureRequest)) { _ =>
        val updatedMerchantAccount = MerchantAccount(
          List(PaymentTransaction(25, "uuid"))
        )
        verify(store).put(s"merchant.$testMerchantId", updatedMerchantAccount.toJson.toString())
      }
    }

    "update card amount" in {
      merchantService.reverse(testMerchantId, testCaptureRequest)
      Thread.sleep(500)
      verify(cardService).charge(testMerchantId, -25)
    }

    "do not reverse if blocked amount less than requested amount" in {
      whenReady(merchantService.reverse(testMerchantId, testCaptureRequest.copy(amount = 100))) { resp =>
        verify(store, never()).put(any[String], any[String])
        resp.message shouldBe "Reverse failed. Available balance too low."
        resp.available shouldBe 50
      }
    }

    "return success reverse" in {
      whenReady(merchantService.reverse(testMerchantId, testCaptureRequest)) { resp =>
        resp.message shouldBe "Reverse successful"
        resp.captured shouldBe 25
        resp.available shouldBe 25
      }
    }

    "do not reverse if transaction not found" in {
      whenReady(merchantService.reverse(testMerchantId, testCaptureRequest.copy(paymentReference = "unknown"))) { resp =>
        verify(store, never()).put(any[String], any[String])
        resp.message shouldBe "Reverse failed. Transaction not found"
      }
    }

    "throw exception when merchant does not exist" in {
      when(store.get(s"merchant.$testMerchantId")).thenReturn(Future.successful(None))
      whenReady(merchantService.reverse(testMerchantId, testCaptureRequest).failed) { resp =>
        resp shouldBe a[RuntimeException]
        resp.getMessage shouldBe "Merchant not found"
      }
    }
  }

  "MerchantService.get" should {
    "return None if merchant not found" in {
      when(store.get(s"merchant.$testMerchantId")).thenReturn(Future.successful(None))
      merchantService.get(testMerchantId).futureValue shouldBe None
    }

    "return merchant" in {
      when(store.get(s"merchant.$testMerchantId")).thenReturn(Future.successful(Some(testMerchantAccount.toJson.toString())))
      merchantService.get(testMerchantId).futureValue.get shouldBe testMerchantAccount
    }

    "throw exception when store has invalid format" in {
      when(store.get(s"merchant.$testMerchantId")).thenReturn(Future.successful(Some("unknown")))
      whenReady(merchantService.get(testMerchantId).failed) { resp =>
        resp shouldBe a[RuntimeException]
        resp.getMessage shouldBe "Something went wrong"
      }
    }
  }
}
