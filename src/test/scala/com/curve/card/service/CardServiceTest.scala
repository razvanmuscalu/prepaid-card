package com.curve.card.service

import java.util.UUID

import com.curve.card.SysBaseTest
import com.curve.card.model.{AuthorisationRequest, CardAccount, LoadRequest}
import org.scalatest.BeforeAndAfterEach
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import spray.json._
import org.mockito.ArgumentMatchers._

import scala.concurrent.Future

class CardServiceTest extends SysBaseTest with MockitoSugar with BeforeAndAfterEach {
  private val testCardId = "uuid-1"
  private val testLoadRequest = LoadRequest(100)
  private val testCardAccount = CardAccount(testCardId, 100, 100)
  private val store = mock[Store]
  private val cardService = new CardServiceImpl(store)

  override protected def beforeEach(): Unit = {
    super.beforeEach()
    reset(store)
    // default mocks (overridden in individual tests where needed)
    when(store.get(any[String])).thenReturn(Future.successful(Some(testCardAccount.toJson.toString)))
    when(store.put(any[String], any[String])).thenReturn(Future.successful(testCardAccount.toJson.toString))
  }

  "CardService.load" should {
    "attempt to retrieve card" in {
      when(store.get(s"card.$testCardId")).thenReturn(Future.successful(Some(testCardAccount.toJson.toString)))
      whenReady(cardService.load(testCardId, testLoadRequest)) { _ =>
        verify(store).get(s"card.$testCardId")
      }
    }

    "return None if card not found" in {
      when(store.get(s"card.$testCardId")).thenReturn(Future.successful(None))
      cardService.load(testCardId, testLoadRequest).futureValue shouldBe None
    }

    "throw exception when store has invalid format" in {
      when(store.get(s"card.$testCardId")).thenReturn(Future.successful(Some("unknown")))
      whenReady(cardService.load(testCardId, testLoadRequest).failed) { resp =>
        resp shouldBe a[RuntimeException]
        resp.getMessage shouldBe "Something went wrong"
      }
    }

    "return updated card" in {
      when(store.get(s"card.$testCardId")).thenReturn(Future.successful(Some(testCardAccount.toJson.toString)))
      val resp = cardService.load(testCardId, testLoadRequest).futureValue.get
      resp.activeAmount shouldBe 200
    }

    "handle negative amount (for reversed payments, top up, etc.)" in {
      when(store.get(s"card.$testCardId")).thenReturn(Future.successful(Some(testCardAccount.toJson.toString)))
      val resp = cardService.load(testCardId, testLoadRequest.copy(amount = -25)).futureValue.get
      resp.activeAmount shouldBe 75
    }
  }

  "CardService.create" should {
    "attempt to create card" in {
      whenReady(cardService.create()) { _ =>
        verify(store).put(any[String], any[String])
      }
    }

    "return created card" in {
      val resp = cardService.create().futureValue
      UUID.fromString(resp.id) // should not throw exception
      resp.activeAmount shouldBe 0
      resp.blockedAmount shouldBe 0
    }
  }

  "CardService.authorise" should {
    "return success if enough active balance" in {
      when(store.get(s"card.$testCardId")).thenReturn(Future.successful(Some(testCardAccount.toJson.toString())))
      when(store.put(any[String], any[String])).thenReturn(Future.successful(""))
      cardService.authorise(testCardId, AuthorisationRequest(50, "uuid")).futureValue.authorised shouldBe true
    }

    "return fail if not enough active balance" in {
      when(store.get(s"card.$testCardId")).thenReturn(Future.successful(Some(testCardAccount.toJson.toString())))
      cardService.authorise(testCardId, AuthorisationRequest(200, "uuid")).futureValue.authorised shouldBe false
    }

    "throw exception when store has invalid format" in {
      when(store.get(s"card.$testCardId")).thenReturn(Future.successful(Some("unknown")))
      whenReady(cardService.authorise(testCardId, AuthorisationRequest(50, "uuid")).failed) { resp =>
        resp shouldBe a[RuntimeException]
        resp.getMessage shouldBe "Something went wrong"
      }
    }

    "return fail if card not found" in {
      when(store.get(s"card.$testCardId")).thenReturn(Future.successful(None))
      cardService.authorise(testCardId, AuthorisationRequest(200, "uuid")).futureValue.authorised shouldBe false
    }

    "update active and blocked balances if authorisation successful" in {
      when(store.get(s"card.$testCardId")).thenReturn(Future.successful(Some(testCardAccount.toJson.toString())))
      when(store.put(any[String], any[String])).thenReturn(Future.successful(""))
      whenReady(cardService.authorise(testCardId, AuthorisationRequest(50, "uuid"))) { _ =>
        verify(store).put(s"card.$testCardId", CardAccount(testCardId, 50, 150).toJson.toString())
      }
    }
  }

  "CardService.get" should {
    "return None if card not found" in {
      when(store.get(s"card.$testCardId")).thenReturn(Future.successful(None))
      cardService.get(testCardId).futureValue shouldBe None
    }

    "return card" in {
      when(store.get(s"card.$testCardId")).thenReturn(Future.successful(Some(testCardAccount.toJson.toString())))
      cardService.get(testCardId).futureValue.get shouldBe testCardAccount
    }

    "throw exception when store has invalid format" in {
      when(store.get(s"card.$testCardId")).thenReturn(Future.successful(Some("unknown")))
      whenReady(cardService.get(testCardId).failed) { resp =>
        resp shouldBe a[RuntimeException]
        resp.getMessage shouldBe "Something went wrong"
      }
    }
  }

  "CardService.charge" should {
    "attempt to retrieve card" in {
      when(store.get(s"card.$testCardId")).thenReturn(Future.successful(Some(testCardAccount.toJson.toString)))
      whenReady(cardService.charge(testCardId, 50)) { _ =>
        verify(store).get(s"card.$testCardId")
      }
    }

    "return None if card not found" in {
      when(store.get(s"card.$testCardId")).thenReturn(Future.successful(None))
      cardService.charge(testCardId, 50).futureValue shouldBe None
    }

    "throw exception when store has invalid format" in {
      when(store.get(s"card.$testCardId")).thenReturn(Future.successful(Some("unknown")))
      whenReady(cardService.charge(testCardId, 50).failed) { resp =>
        resp shouldBe a[RuntimeException]
        resp.getMessage shouldBe "Something went wrong"
      }
    }

    "return updated card" in {
      when(store.get(s"card.$testCardId")).thenReturn(Future.successful(Some(testCardAccount.toJson.toString)))
      val resp = cardService.charge(testCardId, 25).futureValue.get
      resp.blockedAmount shouldBe 75
      resp.activeAmount shouldBe 100
    }

    "handle negative amount (for reversed payments)" in {
      when(store.get(s"card.$testCardId")).thenReturn(Future.successful(Some(testCardAccount.toJson.toString)))
      val resp = cardService.charge(testCardId, -25).futureValue.get
      resp.blockedAmount shouldBe 75
      resp.activeAmount shouldBe 125
    }
  }
}
