package com.curve.card.http

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.ContentTypes.`application/json`
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.curve.card.model.{ExceptionResponse, LoadRequest}
import com.curve.card.service.CardService
import spray.json._

import scala.concurrent.ExecutionContext

object CardRouter {

  def routes(cardService: CardService)(implicit ec: ExecutionContext): Route = load(cardService) ~ create(cardService) ~ retrieve(cardService)

  private def load(cardService: CardService)(implicit ec: ExecutionContext): Route =
    (path("card" / Segment) & put) { id =>
      entity(as[LoadRequest]) { payload =>
        complete {
          cardService.load(id, payload).map {
            case Some(r) => HttpEntity(`application/json`, r.toJson.compactPrint)
            case _       => HttpEntity(`application/json`, ExceptionResponse("Card not found, create a card first").toJson.compactPrint)
          }
        }
      }
    }

  private def create(cardService: CardService)(implicit ec: ExecutionContext): Route =
    (path("card") & post) {
      complete {
        cardService.create().map(resp => HttpEntity(`application/json`, resp.toJson.compactPrint))
      }
    }

  private def retrieve(cardService: CardService)(implicit ec: ExecutionContext): Route =
    (path("card" / Segment) & get) { id =>
      complete {
        cardService.get(id).map {
          case Some(r) => HttpEntity(`application/json`, r.toJson.compactPrint)
          case _       => HttpEntity(`application/json`, ExceptionResponse("Card not found, create a card first").toJson.compactPrint)
        }
      }
    }
}
