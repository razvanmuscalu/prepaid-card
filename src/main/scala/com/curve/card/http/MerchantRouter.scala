package com.curve.card.http

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.ContentTypes.`application/json`
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.curve.card.model.{CaptureRequest, ExceptionResponse, Payment}
import com.curve.card.service.MerchantService
import spray.json._

import scala.concurrent.ExecutionContext

object MerchantRouter {

  def routes(merchantService: MerchantService)(implicit ec: ExecutionContext): Route =
    accept(merchantService) ~ capture(merchantService) ~ retrieve(merchantService) ~ reverse(merchantService)

  private def accept(merchantService: MerchantService)(implicit ec: ExecutionContext): Route =
    (path("merchant" / Segment / "accept") & post) { id =>
      entity(as[Payment]) { payload =>
        complete {
          merchantService.accept(id, payload).map(resp => HttpEntity(`application/json`, resp.toJson.compactPrint))
        }
      }
    }

  private def capture(merchantService: MerchantService)(implicit ec: ExecutionContext): Route =
    (path("merchant" / Segment / "capture") & post) { id =>
      entity(as[CaptureRequest]) { payload =>
        complete {
          merchantService.capture(id, payload).map(resp => HttpEntity(`application/json`, resp.toJson.compactPrint))
        }
      }
    }

  private def reverse(merchantService: MerchantService)(implicit ec: ExecutionContext): Route =
    (path("merchant" / Segment / "reverse") & post) { id =>
      entity(as[CaptureRequest]) { payload =>
        complete {
          merchantService.reverse(id, payload).map(resp => HttpEntity(`application/json`, resp.toJson.compactPrint))
        }
      }
    }

  private def retrieve(merchantService: MerchantService)(implicit ec: ExecutionContext): Route =
    (path("merchant" / Segment) & get) { id =>
      complete {
        merchantService.get(id).map {
          case Some(r) => HttpEntity(`application/json`, r.toJson.compactPrint)
          case _       => HttpEntity(`application/json`, ExceptionResponse("Merchant not found").toJson.compactPrint)
        }
      }
    }
}
