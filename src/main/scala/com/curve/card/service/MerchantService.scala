package com.curve.card.service

import java.util.UUID

import akka.actor.ActorSystem
import akka.stream.Materializer
import com.curve.card.model._
import org.slf4j.{Logger, LoggerFactory}
import spray.json._

import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps
import scala.util.Try

trait MerchantService {
  def accept(id: String, payment: Payment): Future[PaymentOutcome]
  def capture(id: String, capture: CaptureRequest): Future[CaptureOutcome]
  def reverse(id: String, capture: CaptureRequest): Future[CaptureOutcome]
  def get(id: String): Future[Option[MerchantAccount]]
}

class MerchantServiceImpl(cardService: CardService, store: Store)(implicit system: ActorSystem, ec: ExecutionContext, mat: Materializer)
    extends MerchantService {
  private val logger: Logger = LoggerFactory.getLogger(getClass)

  override def accept(id: String, payment: Payment): Future[PaymentOutcome] = {
    val reference = UUID.randomUUID().toString
    val authorisationRequest = AuthorisationRequest(payment.amount, reference)

    def doAccept(merchantAccount: MerchantAccount): Future[PaymentOutcome] =
      cardService.authorise(id, authorisationRequest).flatMap { resp =>
        resp.authorised match {
          case true =>
            val updatedTransactions = PaymentTransaction(payment.amount, reference) :: merchantAccount.transactions
            val updatedMerchantAccount = merchantAccount.copy(transactions = updatedTransactions)
            store.put(s"merchant.$id", updatedMerchantAccount.toJson.toString()).map(_ => PaymentOutcome("Payment successful", reference))
          case false => Future.successful(PaymentOutcome("Payment declined", reference))
        }
      }

    store.get(s"merchant.$id").flatMap {
      case Some(merchant) =>
        Try(merchant.parseJson.convertTo[MerchantAccount]).toOption match {
          case Some(merchantAccount) => doAccept(merchantAccount)
          case _                     => throw new RuntimeException("Something went wrong")
        }
      case _ => doAccept(MerchantAccount(List.empty))
    }
  }

  override def capture(id: String, captureRequest: CaptureRequest): Future[CaptureOutcome] =
    charge(id, captureRequest, "Capture")

  override def reverse(id: String, captureRequest: CaptureRequest): Future[CaptureOutcome] = {
    val reversedCapture = captureRequest.copy(amount = -captureRequest.amount)
    charge(id, reversedCapture, "Reverse")
  }

  private def charge(id: String, captureRequest: CaptureRequest, operation: String): Future[CaptureOutcome] = {
    def doCharge(merchantAccount: MerchantAccount): Future[CaptureOutcome] =
      merchantAccount.transactions.find(_.reference == captureRequest.paymentReference) match {
        case Some(paymentTransaction) =>
          val availableAmount = paymentTransaction.availableAmount - math.abs(captureRequest.amount)
          if (paymentTransaction.availableAmount >= math.abs(captureRequest.amount)) {
            val updatedPaymentTransaction = paymentTransaction.copy(availableAmount = availableAmount)
            val updatedTransactions = updatedPaymentTransaction :: merchantAccount.transactions.filterNot(_.reference == captureRequest.paymentReference)
            val updatedMerchantAccount = merchantAccount.copy(transactions = updatedTransactions)
            cardService.charge(id, captureRequest.amount)
            store
              .put(s"merchant.$id", updatedMerchantAccount.toJson.toString())
              .map(_ => CaptureOutcome(s"$operation successful", math.abs(captureRequest.amount), availableAmount))
          } else {
            Future.successful(CaptureOutcome(s"$operation failed. Available balance too low.", 0, paymentTransaction.availableAmount))
          }
        case _ => Future.successful(CaptureOutcome(s"$operation failed. Transaction not found", 0, 0))
      }

    store.get(s"merchant.$id").flatMap {
      case Some(merchant) =>
        Try(merchant.parseJson.convertTo[MerchantAccount]).toOption match {
          case Some(merchantAccount) => doCharge(merchantAccount)
          case _                     => throw new RuntimeException("Something went wrong")
        }
      case _ => throw new RuntimeException("Merchant not found")
    }
  }

  override def get(id: String): Future[Option[MerchantAccount]] =
    store.get(s"merchant.$id").map {
      case Some(merchant) =>
        Try(merchant.parseJson.convertTo[MerchantAccount]).toOption match {
          case Some(m) => Option(m)
          case _       => throw new RuntimeException("Something went wrong")
        }
      case _ =>
        logger.warn("Merchant not found")
        None
    }
}
