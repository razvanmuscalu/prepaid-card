package com.curve.card.service

import java.util.UUID

import akka.actor.ActorSystem
import akka.stream.Materializer
import com.curve.card.model._
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps
import spray.json._

import scala.util.Try

trait CardService {
  def load(id: String, loadRequest: LoadRequest): Future[Option[CardAccount]]
  def create(): Future[CardAccount]
  def authorise(id: String, authorisationRequest: AuthorisationRequest): Future[AuthorisationResponse]
  def get(id: String): Future[Option[CardAccount]]
  def charge(id: String, amount: Double): Future[Option[CardAccount]]
}

class CardServiceImpl(store: Store)(implicit system: ActorSystem, ec: ExecutionContext, mat: Materializer) extends CardService {
  private val logger: Logger = LoggerFactory.getLogger(getClass)

  override def load(id: String, loadRequest: LoadRequest): Future[Option[CardAccount]] = {
    def doLoad(c: CardAccount): Option[CardAccount] = {
      val updatedC = c.copy(activeAmount = c.activeAmount + loadRequest.amount)
      store.put(s"card.$id", updatedC.toJson.toString)
      Option(updatedC)
    }

    store.get(s"card.$id").map {
      case Some(card) =>
        Try(card.parseJson.convertTo[CardAccount]).toOption match {
          case Some(c) => doLoad(c)
          case _       => throw new RuntimeException("Something went wrong")
        }
      case _ => None
    }
  }

  override def create(): Future[CardAccount] = {
    val id = UUID.randomUUID().toString
    val account = CardAccount(id, 0, 0)
    store.put(s"card.$id", account.toJson.compactPrint)
    Future.successful(account)
  }

  override def authorise(id: String, authorisationRequest: AuthorisationRequest): Future[AuthorisationResponse] = {
    def doAuthorise(c: CardAccount): Future[AuthorisationResponse] =
      if (c.activeAmount >= authorisationRequest.amount) {
        val updatedC = c.copy(
          activeAmount = c.activeAmount - authorisationRequest.amount,
          blockedAmount = c.blockedAmount + authorisationRequest.amount
        )
        store.put(s"card.$id", updatedC.toJson.toString).map(_ => AuthorisationResponse(true))
      } else {
        Future.successful(AuthorisationResponse(false))
      }

    store.get(s"card.$id").flatMap {
      case Some(card) =>
        Try(card.parseJson.convertTo[CardAccount]).toOption match {
          case Some(c) => doAuthorise(c)
          case _       => throw new RuntimeException("Something went wrong")
        }
      case _ => Future.successful(AuthorisationResponse(false))
    }
  }

  override def get(id: String): Future[Option[CardAccount]] =
    store.get(s"card.$id").map {
      case Some(card) =>
        Try(card.parseJson.convertTo[CardAccount]).toOption match {
          case Some(c) => Option(c)
          case _       => throw new RuntimeException("Something went wrong")
        }
      case _ => None
    }

  override def charge(id: String, amount: Double): Future[Option[CardAccount]] = {
    def doUpdate(c: CardAccount): Option[CardAccount] = {
      val updatedBlockedAmount = c.blockedAmount - math.abs(amount)
      val updatedActiveAmount = if (amount < 0) c.activeAmount + -amount else c.activeAmount
      val updatedC = c.copy(activeAmount = updatedActiveAmount, blockedAmount = updatedBlockedAmount)
      store.put(s"card.$id", updatedC.toJson.toString)
      Option(updatedC)
    }

    store.get(s"card.$id").map {
      case Some(card) =>
        Try(card.parseJson.convertTo[CardAccount]).toOption match {
          case Some(c) => doUpdate(c)
          case _       => throw new RuntimeException("Something went wrong")
        }
      case _ => None
    }
  }
}
