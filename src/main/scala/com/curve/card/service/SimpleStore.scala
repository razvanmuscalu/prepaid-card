package com.curve.card.service

import scala.collection.mutable
import scala.concurrent.Future

trait Store {
  def get(key: String): Future[Option[String]]
  def put(key: String, value: String): Future[String]
}

class SimpleStore extends Store {

  val inMem: mutable.Map[String, String] = mutable.Map.empty

  def get(key: String): Future[Option[String]] = inMem.get(key) match {
    case Some(v) => Future.successful(Some(v))
    case _       => Future.successful(None)
  }

  def put(key: String, value: String): Future[String] = {
    inMem.put(key, value)
    Future.successful(value)
  }
}
