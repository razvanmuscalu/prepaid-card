package com.curve.card

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.curve.card.http.{CardRouter, MerchantRouter}
import com.curve.card.service._
import akka.http.scaladsl.server.Directives._

import scala.language.postfixOps

trait AppCake extends Bootstrap with AppConfig {
  this: Sys =>

  private val store: Store = new SimpleStore()
  private val cardService: CardService = new CardServiceImpl(store)
  private val merchantService: MerchantService = new MerchantServiceImpl(cardService, store)
  val route: Route = CardRouter.routes(cardService) ~ MerchantRouter.routes(merchantService)
}

object Main extends App {
  private lazy val system: ActorSystem = ActorSystem()

  val sys = new SysProvider()(system, ActorMaterializer()(system)) with AppCake
  sys.start(sys.route)
}
