package com.curve.card.model

import spray.json.RootJsonFormat

final case class MerchantAccount(transactions: List[PaymentTransaction])

object MerchantAccount {
  import PaymentTransaction.paymentTransactionListFormat
  import spray.json.DefaultJsonProtocol.jsonFormat1

  implicit val merchantAccountFormat: RootJsonFormat[MerchantAccount] = jsonFormat1(MerchantAccount.apply)
}