package com.curve.card.model

import spray.json.RootJsonFormat

final case class AuthorisationRequest(amount: Double, reference: String)

object AuthorisationRequest {
  import spray.json.DefaultJsonProtocol._

  implicit val authorisationRequestFormat: RootJsonFormat[AuthorisationRequest] = jsonFormat2(AuthorisationRequest.apply)
}