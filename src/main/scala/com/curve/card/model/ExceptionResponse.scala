package com.curve.card.model

import spray.json.RootJsonFormat

final case class ExceptionResponse(message: String)

object ExceptionResponse {
  import spray.json.DefaultJsonProtocol._

  implicit val exceptionResponseFormat: RootJsonFormat[ExceptionResponse] = jsonFormat1(ExceptionResponse.apply)
}