package com.curve.card.model

import spray.json.RootJsonFormat

final case class CardAccount(id: String, activeAmount: Double, blockedAmount: Double)

object CardAccount {
  import spray.json.DefaultJsonProtocol._

  implicit val cardAccountFormat: RootJsonFormat[CardAccount] = jsonFormat3(CardAccount.apply)
}