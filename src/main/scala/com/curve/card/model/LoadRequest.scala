package com.curve.card.model

import spray.json.RootJsonFormat

final case class LoadRequest(amount: Double)

object LoadRequest {
  import spray.json.DefaultJsonProtocol._

  implicit val loadRequestFormat: RootJsonFormat[LoadRequest] = jsonFormat1(LoadRequest.apply)
}