package com.curve.card.model

import spray.json.RootJsonFormat

final case class CaptureOutcome(message: String, captured: Double, available: Double)

object CaptureOutcome {
  import spray.json.DefaultJsonProtocol._

  implicit val captureOutcomeFormat: RootJsonFormat[CaptureOutcome] = jsonFormat3(CaptureOutcome.apply)
}