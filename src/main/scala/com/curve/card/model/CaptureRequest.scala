package com.curve.card.model

import spray.json.RootJsonFormat

final case class CaptureRequest(amount: Double, paymentReference: String)

object CaptureRequest {
  import spray.json.DefaultJsonProtocol._

  implicit val captureRequestFormat: RootJsonFormat[CaptureRequest] = jsonFormat2(CaptureRequest.apply)
}