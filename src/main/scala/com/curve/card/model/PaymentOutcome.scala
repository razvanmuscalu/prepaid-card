package com.curve.card.model

import spray.json.RootJsonFormat

final case class PaymentOutcome(message: String, reference: String)

object PaymentOutcome {
  import spray.json.DefaultJsonProtocol._

  implicit val paymentOutcomeFormat: RootJsonFormat[PaymentOutcome] = jsonFormat2(PaymentOutcome.apply)
}