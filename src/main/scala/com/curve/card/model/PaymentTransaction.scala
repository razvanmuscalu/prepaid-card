package com.curve.card.model

import spray.json.RootJsonFormat

final case class PaymentTransaction(availableAmount: Double, reference: String)

object PaymentTransaction {
  import spray.json.DefaultJsonProtocol._

  implicit val paymentTransactionFormat: RootJsonFormat[PaymentTransaction] = jsonFormat2(PaymentTransaction.apply)
  implicit val paymentTransactionListFormat: RootJsonFormat[List[PaymentTransaction]] = listFormat[PaymentTransaction](paymentTransactionFormat)
}