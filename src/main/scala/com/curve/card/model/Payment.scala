package com.curve.card.model

import spray.json.RootJsonFormat

final case class Payment(amount: Double)

object Payment {
  import spray.json.DefaultJsonProtocol._

  implicit val paymentFormat: RootJsonFormat[Payment] = jsonFormat1(Payment.apply)
}