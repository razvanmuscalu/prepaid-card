package com.curve.card.model

import spray.json.RootJsonFormat

final case class AuthorisationResponse(authorised: Boolean)

object AuthorisationResponse {
  import spray.json.DefaultJsonProtocol._

  implicit val authorisationResponseFormat: RootJsonFormat[AuthorisationResponse] = jsonFormat1(AuthorisationResponse.apply)
}