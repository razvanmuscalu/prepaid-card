# prepaid-card

# package and run

`mvn clean package` generates an executable JAR under `target/docker-src/prepaid-card.jar`

`java -jar target/docker-src/prepaid-card.jar` creates a local web server on `localhost:9090`

# API

## card

`POST /card` creates a new card

`PUT /card/<id>` loads amount on card with given ID

```json
{
  "amount": 50
}
```

`GET /card<id>` returns card with given ID

## merchant

`POST /merchant/<id>/accept` accepts a new payment

```json
{
  "amount": 50
}
```

`POST /merchant/<id>/capture` captures the specified amount on the specified accepted payment

```json
{
  "amount": 50,
  "paymentReference": "<id>"
}
```

`POST /merchant/<id>/reverse` reverses the specified amount on the specified accepted payment

```json
{
  "amount": 50,
  "paymentReference": "<id>"
}
```

`GET /merchant/<id>` returns recorded transactions on card with given ID

### Notes

The ID returned when creating a card is the same ID that should be used when accepting, capturing, reversing and getting payments under `/merchant` route.

The ID returned when a new payment is accepted is the same ID that should be used to capture or reverse amounts on the payment.

Please accept apologies for not providing a Swagger file, I ran out of time.

# Summary, Decisions, Limitations

I almost see like this service could be in fact at least 2 services:

 - one to manage the card balances
 - one to manage incoming payments, captures and reverses

Hence why I have two root routes under `/card` and `/merchant`. For the purposes of the exercise I guess it could have been a single simpler service, but that's how I imagined the design before starting the project and I just went with it.

I am aware that the way I store the transactions is not optimal as the object will keep growing as more and more payments are accepted on the card.
I decided at some point along the way to focus on finishing as many features as I can in an MVP way, rather than optimise for more production-like capabilities and risk not completing the required features.
I took this decision on the assumption that this would be only an MVP to demonstrate the features. If this would be an MVP that would have to be deployed in production, then certainly I would optimise further this aspect of the application.

However, that being said, the `Store` interface I provided can easily have a Redis instance instead of the `SimpleStore` using maps. I was actually thinking before I started the project to do this in Redis, but thought it would require more time so I decided to build the main services `CardService` and `MerchantService` in a Redis compatible way but  by using maps.

I ran out of time trying to implement the second bonus transactions feature. However, the `GET /merchant/<id>` does return a transaction log.
Given the incoming `Payment` object would be more complete (containing information like merchant, original amount, category, etc.), then this endpoint would return a more complete transaction log  by default.


